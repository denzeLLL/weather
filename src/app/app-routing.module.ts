import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentLayoutComponent} from "./layouts/content-layout/content-layout.component";

const routes:Routes = [
    {
        path: '',
        component: ContentLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'weather',
                pathMatch: 'full'
            },
            {
                path: 'about',
                loadChildren: './modules/about/about.module#AboutModule'
            },
            {
                path: 'weather',
                loadChildren: './modules/weather/weather.module#WeatherModule'
            }
        ]
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
