import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ContentLayoutComponent} from "./layouts/content-layout/content-layout.component";
import {NavComponent} from "./layouts/nav/nav.component";
import {SharedModule} from "./shared/shared.module";

@NgModule({
    declarations: [
        AppComponent,
        ContentLayoutComponent,
        NavComponent
    ],
    imports: [
        BrowserModule,

        SharedModule,

        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
