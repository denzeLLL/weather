import {NgModule} from '@angular/core';
import {AboutComponent} from './pages/about/about.component';
import {AboutRoutingModule} from "./about-routing.module";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
    declarations: [AboutComponent],
    imports: [
        AboutRoutingModule,
        
        SharedModule
    ]
})
export class AboutModule {
}
