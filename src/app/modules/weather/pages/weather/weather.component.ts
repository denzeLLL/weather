import {Component, OnInit} from '@angular/core';
import {GetWeatherService} from "../../../../shared/services/get-weather.service";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

    public typeChart = 'LineChart';
    public city = new FormControl();
    public dataChart = [];

    constructor(private getWeatherService: GetWeatherService) {
    }

    ngOnInit() {
        let self = this;
        self.city.valueChanges
            .pipe(
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(
                function (data) {
                    self.changeInput(data)
                },
                function (error) {
                    console.log('error: ', error);
                }
            );
    }

    changeInput(val) {
        let self = this;
        self.getWeatherService.getWeatherByCity({city: val})
            .subscribe(
                function (data:any) {
                    self.dataChart = data.list.map(item => {
                        return [item.dt_txt, item.main.temp]
                    });
                },
                function (error) {
                    console.log('error: ', error);
                }
            );
    }

}

