import {NgModule} from '@angular/core';
import {WeatherComponent} from './pages/weather/weather.component';
import {WeatherRoutingModule} from "./weather-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {GoogleChartsModule} from 'angular-google-charts';

@NgModule({
    declarations: [WeatherComponent],
    imports: [
        WeatherRoutingModule,

        GoogleChartsModule.forRoot(),

        SharedModule
    ]
})
export class WeatherModule {
}
