import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class GetWeatherService {

    private APPID = 'ae51a0d72e8682383aacc5641c81ca18';

    constructor(private httpClient:HttpClient) {
    }

    private handleError(data:any) {
        const errorMsg = (data.error.message) ? data.error.message :
            data.error.status ? `${data.error.status} - ${data.error.statusText}` : 'Server error';
        return throwError(errorMsg);
    }


    public getWeatherByCity({city = ''} = {}) {
        return this.httpClient.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${this.APPID}&units=metric`)
            .pipe(
                catchError(this.handleError)
            );
    }
}
